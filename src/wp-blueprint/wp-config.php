/************************* START CUSTOM WP-CONFIG *************************/
// Define the number of post/page revisions
define( 'WP_POST_REVISIONS', 3 );
define( 'WP_PAGE_REVISIONS', 3 );

// Zet op true om wp-admin te blokeren voor gebruikers buiten Sebwite
define( 'DISABLE_WP_ADMIN' , false);

// Define the current environment, possible values 'development', 'staging' or 'production' (live stage).
define('SEB_ENVIRONMENT', 'development');

if ((defined('SEB_ENVIRONMENT')) && (SEB_ENVIRONMENT == 'development' ) || (SEB_ENVIRONMENT == 'staging' )) {

    // Enable WP_DEBUG mode
    define('WP_DEBUG', true);

    // Enable Debug logging to the /wp-content/debug.log file
    define('WP_DEBUG_LOG', true);

    // Disable display of errors and warnings 
    define('WP_DEBUG_DISPLAY', false);

    // Save all SQL queries
    define('SAVEQUERIES', true);
}
else if ((defined('SEB_ENVIRONMENT')) && (SEB_ENVIRONMENT != 'development')) {

    // Disable WP_DEBUG mode
    define('WP_DEBUG', false);
}
/************************* END CUSTOM WP-CONFIG *************************/