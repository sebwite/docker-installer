#!/bin/bash

# Enable and set aliasses
shopt -s expand_aliases
alias wp="wp --allow-root"

# Version: 0.5

HTTPDOCS=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
START_SECONDS="$(date +%s)"
SERVER_URL="http://192.168.1.100"

function soft_quit {
	END_SECONDS="$(date +%s)"
	echo "-----------------------------"
	echo "Voltooid in "$(expr $END_SECONDS - $START_SECONDS)" seconden"

	exit
}

read -p "Wil je beginnen met een schone WordPress installatie (y/n)? " answer
case ${answer:0:1} in
    y|Y)

		echo "WordPress installeren"

		# Get user variables
		read -e -p "Maak een map aan:" -i "" FILE_PATH
		read -e -p "Welke WordPress versie wil je installeren: (laat leeg voor de laatste versie) " -i "" WPVERSION
		read -e -p "Wat is de titel van de website?: " -i "" WPTITLE

		if [ -d "$HTTPDOCS/$FILE_PATH" ]; then
			# Directory exists
			echo "Map bestaat al"
			soft_quit
		fi
		mkdir $FILE_PATH
		cd $FILE_PATH

		# Copy blueprint wp-cli.yml
		echo "Kopieer wp-cli.yml"
		cp "${HTTPDOCS}/wp-blueprint/wp-cli.yml" "${HTTPDOCS}/${FILE_PATH}"


		if [ -n "$WPVERSION" ]; then
			PARAMVERSION="--version=${WPVERSION}"
		else
			PARAMVERSION=" "
		fi

		wp core download --locale=nl_NL $PARAMVERSION
		wp core config --dbname=$MYSQL_DATABASE --dbhost=$MYSQL_CONTAINER --dbuser=$MYSQL_USER --dbpass=$MYSQL_ROOT_PASSWORD --locale=nl_NL --extra-php < "${HTTPDOCS}/wp-blueprint/wp-config.php"
		wp db create
		wp core install --url="${SERVER_URL}/${FILE_PATH}" --title="${WPTITLE}" --admin_user=sebadmin --admin_password=Sebwp123 --admin_email=wordpress@mail.sebwite.nl

		echo "Download basisthema"
		cd wp-content/themes

		# We only want the latest version, not the whole repository history
		git clone --depth=1 --branch=master https://bitbucket.org/sebwite/basisthema
		# Move themes to correct folder, then delete repo
		mv basisthema/sebwite.base-theme-child "${HTTPDOCS}/${FILE_PATH}/wp-content/themes"
		mv basisthema/sebwite.base-theme "${HTTPDOCS}/${FILE_PATH}/wp-content/themes"
		rm -rf basisthema

		# Delete default themes
		echo "Verwijder standaard thema's"
		rm -rf twentythirteen twentyfourteen

		# Delete default plugins
		echo "Verwijder Hello Dolly en Akismet"
		wp plugin delete hello akismet

		# Install plugins
		cd "${HTTPDOCS}/${FILE_PATH}/wp-content/plugins"

		# WP Sync DB
			git clone --depth=1 https://github.com/wp-sync-db/wp-sync-db wp-sync-db
			# Delete .git files
			rm -rf wp-sync-db/.git

		# WP Sync DB Media Files
			git clone --depth=1 https://github.com/wp-sync-db/wp-sync-db-media-files wp-sync-db-media-files
			# Delete .git files
			rm -rf wp-sync-db-media-files/.git

		# WordPress importer
			wp plugin install wordpress-importer

		# Update permalinks
		echo "Permalinks instellen"
		wp rewrite structure --hard '/%year%/%monthnum%/%postname%'
		wp rewrite flush --hard

		# Activate plugins
		wp plugin activate wp-sync-db wp-sync-db-media-files wordpress-importer


		# Copy blueprint .htaccess en robots.txt
		echo "Kopieer .htaccess en robots.txt"
		cp "${HTTPDOCS}/wp-blueprint/.htaccess" "${HTTPDOCS}/${FILE_PATH}"
		cp "${HTTPDOCS}/wp-blueprint/robots.txt" "${HTTPDOCS}/${FILE_PATH}"

		# We are done!
		echo "WordPress is succesvol geinstalleerd, ga naar ${SERVER_URL}/${FILE_PATH} om de website te bekijken"
		soft_quit

    ;;
    n|N )
        echo "Bestaande WordPress clonen"

        # Get user variables
        read -e -p "Wat is de .git URL: " -i "" WPCLONE
   		read -e -p "Wat wordt de projectmap: " -i "" FILE_PATH

   		# Check if folder exists
		if [ -d "$HTTPDOCS/$FILE_PATH" ]; then
			echo "Map bestaat al"
			soft_quit
		fi

		# Clone repository
		git clone --depth=1 $WPCLONE $FILE_PATH
        cd $FILE_PATH

		# Copy blueprint wp-cli.yml
		echo "Kopieer wp-cli.yml"
		cp "${HTTPDOCS}/wp-blueprint/wp-cli.yml" "${HTTPDOCS}/${FILE_PATH}"

        # Check if WP is installed, if not, install
        if ! $(wp core is-installed); then

	    	echo "WordPress installeren"
			wp core config --dbname="${START_SECONDS}_${FILE_PATH}" --dbuser=productie --dbpass=sebhq123 --locale=nl_NL --extra-php < "${HTTPDOCS}/wp-blueprint/wp-config.php"
			wp db create
			wp core install --url="${SERVER_URL}/${FILE_PATH}" --title="${FILE_PATH}" --admin_user=sebadmin --admin_password=Sebwp123 --admin_email=wordpress@mail.sebwite.nl

	    fi

		# Install plugins
		cd "${HTTPDOCS}/${FILE_PATH}/wp-content/plugins"

		# WP Sync DB
			# Remove plugin if allready installed
			rm -rf wp-sync-db
			git clone --depth=1 https://github.com/wp-sync-db/wp-sync-db wp-sync-db
			# Delete .git files
			rm -rf wp-sync-db/.git

		# WP Sync DB Media Files
			# Remove plugin if allready installed
			rm -rf wp-sync-db-media-files
			git clone --depth=1 https://github.com/wp-sync-db/wp-sync-db-media-files wp-sync-db-media-files
			# Delete .git files
			rm -rf wp-sync-db-media-files/.git

		# Activate plugins
		wp plugin activate wp-sync-db wp-sync-db-media-files

		# Update permalinks
		echo "Permalinks updaten"
		wp rewrite flush --hard

		# We are done!
		echo "WordPress is succesvol geinstalleerd, ga naar ${SERVER_URL}/${FILE_PATH} om een database migrate pull te doen"
		soft_quit
    ;;
    * )
    	echo "Onjuiste waarde"
    	soft_quit
    ;;
esac