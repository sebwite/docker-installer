FROM ubuntu:14.04
MAINTAINER Thijs van den Berg <thijs@sebwite.nl>

# Update APT
RUN apt-get update -y

# Base stuff
RUN apt-get install -y --force-yes curl git

# Install editors
RUN apt-get install -y --force-yes vim nano

RUN apt-get update -y && apt-get install php5-cli php5-mysql mysql-client -y

WORKDIR /root

RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod +x wp-cli.phar
RUN sudo mv wp-cli.phar /usr/local/bin/wp

RUN curl -O https://raw.githubusercontent.com/wp-cli/wp-cli/master/utils/wp-completion.bash

RUN echo "source /root/wp-completion.bash" >> /etc/profile
RUN echo "alias wp=\"wp --allow-root\"" >> /etc/profile
RUN echo "alias wp=\"wp --allow-root\"" >> /root/.bashrc

VOLUME ["/src"]
WORKDIR /src
ENTRYPOINT ["./install.sh"]