# Readme


Create MySQL container
```
docker run --name docker-installer-mysql --env-file docker.env -d mysql:5.7
```

Run installer
```
docker run -it --name docker-installer --volume="$PWD/src/":/src --link docker-installer-mysql --env-file docker.env docker-installer
```

Run container without installer
```
docker run -it --name docker-installer --volume="$PWD/src/":/src --link docker-installer-mysql --env-file docker.env docker-installer /bin/bash
```

Connect to MySQL container and check if DB is created
```
mysql -h $MYSQL_CONTAINER_ -u$MYSQL_USER -p$MYSQL_ROOT_PASSWORD
show databases;
```

